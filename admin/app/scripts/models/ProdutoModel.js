'use strict';

app.factory('ProdutoModel', function (ProdutoService, CategoriaModel) {
    var self = this;

    self.getProdutos = function (callback) {
        ProdutoService.getProdutos()
                .success(function (response) {
                    return callback(response, true);
                })
                .error(function (response) {
                    return callback(response, false);
                });
    };

    self.getProduto = function (produtos, idProduto, callback) {
        angular.forEach(produtos, function (produto, key) {
            if (produto.idProduto === idProduto) {
                return callback(produto, key);
            }
        });
    };

    self.new = function () {
        return {
            idProduto: null,
            idCategoriaProduto: CategoriaModel.new(),
            nomeProduto: null,
            descricaoProduto: null,
            qtdeProduto: null,
            valorProduto: null
        };
    };

    self.save = function (produto, callback) {
        ProdutoService.saveProduto(produto)
                .success(function (response) {
                    return callback(response, true);
                })
                .error(function (response) {
                    return callback(response, false);
                });
    };
    
    self.delete = function (idProduto, callback) {
        ProdutoService.delete(idProduto)
                .success(function (response) {
                    return callback(response, true);
                })
                .error(function (response) {
                    return callback(response, false);
                });
    };

    return self;
});
