'use strict';

app.factory('CategoriaModel', function (CategoriaService) {
    var self = this;

    self.getCategorias = function (callback) {
        CategoriaService.getCategorias()
                .success(function (response) {
                    return callback(response, true);
                })
                .error(function (response) {
                    return callback(response, false);
                });
    };

    self.getCategoria = function (categorias, idCategoria, callback) {
        angular.forEach(categorias, function (categoria, key) {
            if (categoria.idCategoria === idCategoria) {
                return callback(categoria, key);
            }
        });
    };

    self.new = function () {
        return {
            idCategoria: null,
            nomeCategoria: null,
            descricaoCategoria: null
        };
    };

    self.save = function (categoria, callback) {
        CategoriaService.saveCategoria(categoria)
                .success(function (response) {
                    return callback(response, true);
                })
                .error(function (response) {
                    return callback(response, false);
                });
    };

    return self;
});
