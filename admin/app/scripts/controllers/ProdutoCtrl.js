'use strict';
app.controller('ProdutoCtrl', function ($scope, ProdutoModel, CategoriaModel, ngDialog) {

    $scope.produtos = [];
    $scope.categorias = [];
    $scope.key = false;
    $scope.keyCategoria = false;
    $scope.message = false;
    $scope.textSearch = "";


    ProdutoModel.getProdutos(function (response, status) {
        if (status) {
            $scope.produtos = response;
        } else {
            console.log(response);
        }
    });

    CategoriaModel.getCategorias(function (response, status) {
        if (status) {
            $scope.categorias = response;
        } else {
            console.log(response);
        }
    });

    $scope.edit = function (idProduto) {
        if (typeof idProduto == 'undefined') {
            $scope.key = $scope.produtos.length + 1;
            $scope.produtos[$scope.key] = ProdutoModel.new();
            openModalProduto();
        } else {
            ProdutoModel.getProduto($scope.produtos, idProduto, function (produto, key) {
                $scope.key = key;
                openModalProduto();
            });
        }
    };

    $scope.save = function () {
        ProdutoModel.save($scope.produtos[$scope.key], function (response, status) {
            if (status) {
                $scope.produtos[$scope.key] = response;
                $scope.message = "Salvo com sucesso";
            } else {
                console.log(response);
                $scope.message = "Falha na execução: " + response;
            }
        });
    };

    $scope.delete = function () {
        ProdutoModel.delete($scope.produtos[$scope.key].idProduto, function (response, status) {
            if (status) {
                delete $scope.produtos[$scope.key];
                $scope.message = "Excluido com sucesso!";
                closeModal();
            } else {
                console.log(response);
                $scope.message = "Falha na execução: " + response;
            }
        });
    };

    $scope.editCategorias = function (idCategoria) {
        if (typeof idCategoria == 'undefined') {
            $scope.keyCategoria = $scope.categorias.length + 1;
            $scope.categorias[$scope.keyCategoria] = CategoriaModel.new();
            console.log($scope.categorias[$scope.keyCategoria] = CategoriaModel.new());
            openModalCategorias();
        } else {
            CategoriaModel.getCategoria($scope.categorias, idCategoria, function (categoria, key) {
                $scope.keyCategoria = key;
                console.log($scope.keyCategoria);
                openModalCategorias();
            });
        }
    };

    $scope.saveCategoria = function () {
        CategoriaModel.save($scope.categorias[$scope.keyCategoria], function (response, status) {
            if (status) {
                $scope.categorias[$scope.keyCategoria] = response;
                $scope.message = "Salvo com sucesso";
            } else {
                console.log(response);
                $scope.message = "Falha na execução: " + response;
            }
        });
    };

    $scope.deleteCategoria = function () {
        CategoriaModel.delete($scope.categorias[$scope.keyCategoria].idProduto, function (response, status) {
            if (status) {
                delete $scope.categorias[$scope.keyCategoria];
                $scope.message = "Excluido com sucesso!";
                closeModal();
            } else {
                console.log(response);
                $scope.message = "Falha na execução: " + response;
            }
        });
    };

    $scope.next = function () {
        $scope.key++;
        $scope.message = false;
    };

    $scope.prev = function () {
        $scope.key--;
        $scope.message = false;
    };

    $scope.nextCategoria = function () {
        $scope.keyCategoria++;
        $scope.message = false;
    };

    $scope.prevCategoria = function () {
        $scope.keyCategoria--;
        $scope.message = false;
    };

    $scope.searchCategoria = function () {
        console.log($scope.textSearch);
        CategoriaModel.getCategoria($scope.categorias, $scope.textSearch, function (categoria, key) {
            console.log(categoria);
            $scope.keyCategoria = key;
        });
    };

    var openModalProduto = function () {
        $scope.message = false;
        ngDialog.open({
            template: 'views/produtos/produto.html',
            className: 'ngdialog-theme-default custom-width',
            scope: $scope
        });
    };

    var openModalCategorias = function () {
        console.log($scope.categorias);
        $scope.message = false;
        ngDialog.open({
            template: 'views/categorias/categoria.html',
            className: 'ngdialog-theme-default custom-width',
            scope: $scope
        });
    };

    var closeModal = function () {
        ngDialog.close();
    };

});