
app.service('CategoriaService', function ($http, config) {

    var self = this;

    self.getCategorias = function (params) {
        if (typeof params == 'undefined') {
            params = "";
        }
        return $http.get(config.baseUrl + "categoria?" + params);
    };

    self.getCategoriaById = function (idCategoria) {
        return $http.get(config.baseUrl + "categoria/" + idCategoria);
    };

    self.saveCategoria = function (categoria) {
        if (categoria.idCategoria > 0) {
            return $http.put(config.baseUrl + "categoria/" + categoria.idCategoria, categoria);//update
        } else {
            return $http.post(config.baseUrl + "categoria", categoria);//insert
        }
    };

    self.delete = function (idCategoria) {
        return $http.delete(config.baseUrl + "categoria/" + idCategoria);//update
    };

    return self;

});
