
app.service('ProdutoService', function ($http, config) {

    var self = this;

    self.getProdutos = function (params) {
        if (typeof params == 'undefined') {
            params = "";
        }
        return $http.get(config.baseUrl + "produto?" + params);
    };

    self.getProdutoById = function (idProduto) {
        return $http.get(config.baseUrl + "produto/" + idProduto);
    };

    self.saveProduto = function (produto) {
        if (produto.idProduto > 0) {
            return $http.put(config.baseUrl + "produto/" + produto.idProduto, produto);//update
        } else {
            return $http.post(config.baseUrl + "produto", produto);//insert
        }
    };

    self.delete = function (idProduto) {
        return $http.delete(config.baseUrl + "produto/" + idProduto);//update
    };

    return self;

});
