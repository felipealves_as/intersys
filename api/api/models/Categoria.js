/**
* Categoria.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
    tableName: 'categoria',
    attributes: {
        idCategoria: {
            columnName: 'id_categoria',
            type: 'integer',
            primaryKey: true,
            autoIncrement: true,
        },
        nomeCategoria: {
            columnName: 'nome_categoria',
            type: 'string',
            required: true,
            size: 255,
        },
        descricaoCategoria: {
            columnName: 'descricao_categoria',
            type: 'string'
        }
    }
};

