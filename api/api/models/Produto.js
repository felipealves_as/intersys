/**
* Produto.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
    tableName: 'produto',
    attributes: {
        idProduto: {
            columnName: 'id_produto',
            type: 'integer',
            primaryKey: true,
            autoIncrement: true
        },
        nomeProduto: {
            columnName: 'nome_produto',
            type: 'string',
            required: true,
            size: 255
        },
        descricaoProduto: {
            columnName: 'descricao_produto',
            type: 'string'
        },
        qtdeProduto: {
            columnName: 'qtde_produto',
            type: 'integer'
        },
        valorProduto: {
            columnName: 'valor_produto',
            type: 'float',
            size: 12,
            required: true
        },
        idCategoriaProduto: {
            columnName: 'id_categoria_produto',
            type: 'integer',
            required: true,
            model: 'Categoria'
        }
    }
};
